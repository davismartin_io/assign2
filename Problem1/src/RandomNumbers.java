
//************************************************
// File:        Problem 1
// Author:      Davis Martin
// Date:        3 October 2018
// Course:      CPS 100
//
// PROBLEM STATEMENT:Write a program that creates and
// prints a a random phone number of the form XXX-XXX-XXXX.
// Include the dashes in the output. Do not let first three
// digits contain an 8 or 9 (but don't be more restrictive
// that that), and make sure that the second set of three
// digits is not greater than 655. 
// ADDENDUM TO PROBLEM: 
// INPUT: A random number generator that imitates a phone number.
// The first set of numbers are not able to individually turn into 
// a 8 or a 9 with the constraints given. The second set is constraint 
// to not output above 655, and the third set is made to always be 4 digits. 
//
// OUTPUT: This will output the first set within parentheses (like most phone numbers)
// with numbers that are not 8 or 9 Followed by a dash. The second set is 3 digits
// that are never above 655, also followed by a dash. The third set is 4 digits long with
// no constraints other than not going below 4 digits. 
//*************************************************

import java.util.Random;

public class RandomNumbers {

	public static void main(String[] args) {

		Random generator = new Random();
	//Setting the initial values to be determined to a random number.
		int FirstSetNumber1;
		int FirstSetNumber2;
		int FirstSetNumber3;
		int SecondSetNumber;
		int ThirdSetNumber;
	//setting the constraints without magic numbers
		int SetOneConstraint = 7;
		int SetTwoConstraint = 655;
		int SetThreeConstraint = 9999;
		int ThreeDigitConstraint = 100;
		int FourDigitConstraint = 1000;

	//Creating the Randomly generated numbers for the output values. 
		FirstSetNumber1 = generator.nextInt(SetOneConstraint);
		FirstSetNumber2 = generator.nextInt(SetOneConstraint);
		FirstSetNumber3 = generator.nextInt(SetOneConstraint);
		SecondSetNumber = generator.nextInt(SetTwoConstraint) + ThreeDigitConstraint;
		ThirdSetNumber = generator.nextInt(SetThreeConstraint) + FourDigitConstraint;
	// Printing results, with edits of dashes and parentheses.
		System.out.print("(" + FirstSetNumber1 + FirstSetNumber2 + FirstSetNumber3 );
		System.out.print(")-" + SecondSetNumber);
		System.out.print("-" + ThirdSetNumber);

	}

}
