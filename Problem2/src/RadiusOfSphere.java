
//************************************************
// File:        Problem 2
// Author:      Davis Martin
// Date:        4 October 2018
// Course:      CPS 100
//
// PROBLEM STATEMENT: Write a program that reads the radius of a sphere
// and prints its volume and surface area.
//  
// INPUT: Input any radius for a volume and area. 

// OUTPUT: Prints the calculated volume and area from the users requested number. 
//*************************************************

import java.text.DecimalFormat;
import java.util.Scanner;

public class RadiusOfSphere {
	public static void main(String[] args) {

		Scanner Myscan = new Scanner(System.in);
		// A class created to output 4 decimal places for volume and area.
		DecimalFormat DForm = new DecimalFormat("0.####");
		// Have no value until the value of the radius is inputed.
		double Area;
		double Volume;
		double Radius;

		System.out.println("Enter the radius:");
		Radius = Myscan.nextDouble();
		Myscan.close();

		Volume = (4.00 / 3) * Math.PI * Math.pow(Radius, 3);
		Area = 4 * Math.PI * Math.pow(Radius, 2);

		System.out.println("Volume = " + DForm.format(Volume));
		System.out.println("Area = " + DForm.format(Area));

	}

}
